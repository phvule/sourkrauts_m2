var config = {
    paths: {
        'theme'       : 'js/theme',
        'owl_carousel': 'js/owl-carousel/owl.carousel',
        'enquire'     : 'js/enquire/enquire',
    },
    shim: {

        'owl_carousel': {
            deps: ['jquery']
        },

        'theme': {
            deps: ['jquery','owl_carousel']
        },

        'enquire': {
            deps: ['jquery']
        },
    }
};
