define(
    ["jquery",
        'owl_carousel',
    ], function ($) {
    var banner = $('.banner-carousel');
    banner.owlCarousel({
        items:1,
        loop:true,
        autoplay: true,
        autoplaySpeed: 2500,
        autoplayTimeout: 7000,
        autoplayHoverPause:true,
        nav: true,
        dots: true,
    });

    $('.account-carousel').owlCarousel({
        items:1,
        loop:true,
        autoplay: false,
        autoplaySpeed: 2500,
        autoplayTimeout: 7000,
        autoplayHoverPause:true,
        nav: true,
        dots: true,
    });

});
