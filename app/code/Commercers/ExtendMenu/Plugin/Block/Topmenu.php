<?php

namespace Commercers\ExtendMenu\Plugin\Block;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Layer\Resolver;

/**
 * Class Topmenu
 *
 */
class Topmenu
{
    /**
     * @var Resolver
     */
    private $layerResolver;

    /**
     * Topmenu constructor.
     * @param Resolver $layerResolver
     */
    public function __construct(
        Resolver $layerResolver
    ) {
        $this->layerResolver = $layerResolver;
    }

    /**
     * Get current Category from catalog layer
     *
     * @return \Magento\Catalog\Model\Category
     */
    private function getCurrentCategory()
    {
        $catalogLayer = $this->layerResolver->get();

        if (!$catalogLayer) {
            return null;
        }

        return $catalogLayer->getCurrentCategory();
    }

    /**
     * Add category id to cache tag
     *
     * @param \Magento\Theme\Block\Html\Topmenu $subject
     * @param array $result
     * @return array
     */
    public function afterGetCacheKeyInfo(\Magento\Theme\Block\Html\Topmenu $subject, $result)
    {
        $category = $this->getCurrentCategory();

        if (!$category || !$category->getId()) {
            return $result;
        }

        $result[] = Category::CACHE_TAG . '_' . $category->getId();

        return $result;
    }
}

