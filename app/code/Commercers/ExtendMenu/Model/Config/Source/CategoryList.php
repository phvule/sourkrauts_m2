<?php

namespace Commercers\ExtendMenu\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Catalog\Helper\Category;
use Magento\Catalog\Model\CategoryRepository;

class Categorylist implements ArrayInterface
{
    protected $_categoryHelper;

    protected $categoryRepository;

    public function __construct(
        Category $catalogCategory,
        CategoryRepository $categoryRepository
    )
    {
        $this->_categoryHelper = $catalogCategory;
        $this->categoryRepository = $categoryRepository;
    }
    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
    {
        return $this->_categoryHelper->getStoreCategories($sorted , $asCollection, $toLoad);
    }
    public function toOptionArray()
    {
        $arr = $this->toArray();
        $ret = [];
        foreach ($arr as $key => $value)
        {
            $ret[] = [
                'value' => $key,
                'label' => $value
            ];
        }

        return $ret;
    }
    public function toArray()
    {
        $categories = $this->getStoreCategories(true,false,true);
        $categoryList = $this->renderCategories($categories);
        return $categoryList;
    }
    public function renderCategories($_categories)
    {
        $categoryList = array();
        foreach ($_categories as $category){
            $categoryList[$category->getEntityId()] = __($category->getName()); // Main categories
            $categoryObj = $this->categoryRepository->get($category->getId());
            $subcategories = $categoryObj->getChildrenCategories();
            foreach($subcategories as $subcategory) {
                $categoryList[$subcategory->getEntityId()] = __($category->getName().'-->'.$subcategory->getName());  // 1st level Sub categories
                if($subcategory->hasChildren()) {
                    $childCategoryObj = $this->categoryRepository->get($subcategory->getId());
                    $childSubcategories = $childCategoryObj->getChildrenCategories();
                    foreach($childSubcategories as $childSubcategory) {
                        $categoryList[$childSubcategory->getEntityId()] = __($category->getName().'-->'.$subcategory->getName().'-->'.$childSubcategory->getName());  // 2nd level Sub categories
                    }
                }
            }
        }

        return $categoryList;
    }

}