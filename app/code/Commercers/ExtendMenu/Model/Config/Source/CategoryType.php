<?php

namespace Commercers\ExtendMenu\Model\Config\Source;

class CategoryType
{
    const CATEGORY_WOMEN = 'women';

    const CATEGORY_MEN = 'men';
}
