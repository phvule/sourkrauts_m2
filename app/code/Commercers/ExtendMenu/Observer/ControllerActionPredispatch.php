<?php

namespace Commercers\ExtendMenu\Observer;

use Magento\Cms\Api\Data\PageInterface;
use Magento\Cms\Model\ResourceModel\Page;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Commercers\ExtendMenu\Helper\Data;
use Magento\Framework\App\Request\Http;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Cms\Model\PageFactory;
use Commercers\ExtendMenu\Model\Config\Source\CategoryType;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Store\Model\StoreManagerInterface;

class ControllerActionPredispatch implements ObserverInterface
{
    protected $_customerSession;

    protected $_request;

    protected $_dataHelper;

    protected $_urlInterFace;

    protected $_pageFactory;

    protected $_productFactory;

    protected $_storeManager;

    protected $_pageResource;

    protected $_categoryFactory;

    public function __construct(
        Http $request,
        SessionFactory $customerSession,
        PageFactory$pageFactory,
        Data $dataHelper,
        StoreManagerInterface $storeManager,
        UrlInterface $urlInterFace,
        ProductFactory $productFactory,
        Page $pageResource,
        CategoryFactory $categoryFactory

    ){
        $this->_categoryFactory = $categoryFactory;
        $this->_pageResource = $pageResource;
        $this->_storeManager = $storeManager;
        $this->_urlInterFace = $urlInterFace;
        $this->_dataHelper = $dataHelper;
        $this->_request = $request;
        $this->_customerSession = $customerSession;
        $this->_pageFactory = $pageFactory;
        $this->_productFactory = $productFactory;
    }
    public function execute(Observer $observer)
    {
        $moduleName = $this->_request->getModuleName();
        $controller = $this->_request->getControllerName();

        if(!$this->_request->isAjax()) {
            $isUnsetSession = false;
            if ($moduleName == "cms" && $controller =="page"){
                $pageId = $this->_request->getParam('page_id');
                if($pageId) {
                    $cmsPageWomen = $this->_dataHelper->getCmsPageWomen();
                    $cmsPageMen = $this->_dataHelper->getCmsPageMen();
                    $pageFactory = $this->_pageFactory->create()->load($pageId);
                    $storeId = $this->_storeManager->getStore()->getId();
                    $page = $this->_pageFactory->create();
                    $page->setStoreId($storeId);

                    if ($pageFactory->getIdentifier() == $cmsPageWomen ){
                        $this->_customerSession->create()->setCategoryType(CategoryType::CATEGORY_WOMEN);
                    } elseif ($pageFactory->getIdentifier() == $cmsPageMen){
                        $this->_customerSession->create()->setCategoryType(CategoryType::CATEGORY_MEN);
                    } else {
                        $isUnsetSession = true;
                    }
                }
            } elseif ($moduleName == "catalog" && $controller =="category") {
                $categoryId = $this->_request->getParam('id');
                $category = $this->_categoryFactory->create()->load($categoryId);
                $categoryIds = $category->getParentId();

                $womenRootCategory = $this->_dataHelper->getWomenRootCategory();
                $menRootCategory = $this->_dataHelper->getMenRootCategory();

                if($categoryIds == $womenRootCategory ) {
                    $this->_customerSession->create()->setCategoryType(CategoryType::CATEGORY_WOMEN);
                }elseif($categoryIds == $menRootCategory) {
                    $this->_customerSession->create()->setCategoryType(CategoryType::CATEGORY_MEN);
                }
            } elseif ($moduleName == "catalog" && $controller =="product") {
                $productId = $this->_request->getParam('id');
                $product = $this->_productFactory->create()->load($productId);
                $categoryIds = $product->getCategoryIds();

                $womenRootCategory = $this->_dataHelper->getWomenRootCategory();
                $menRootCategory = $this->_dataHelper->getMenRootCategory();

                if(in_array($womenRootCategory,$categoryIds)) {
                    $this->_customerSession->create()->setCategoryType(CategoryType::CATEGORY_WOMEN);
                }elseif(in_array($menRootCategory,$categoryIds)) {
                    $this->_customerSession->create()->setCategoryType(CategoryType::CATEGORY_MEN);
                }
            }elseif (
                $moduleName && $controller &&
                !($moduleName == "catalog" && $controller =="category") &&
                !($moduleName == "catalog" && $controller =="product")
            ){
                $isUnsetSession = true;
            }

            if($isUnsetSession) {
                $this->_customerSession->create()->unsCategoryType();
            }
        }
    }

}
