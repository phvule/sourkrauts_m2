<?php

namespace Commercers\ExtendMenu\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\SessionFactory;
use Commercers\ExtendMenu\Model\Config\Source\CategoryType;
use Magento\Cms\Api\Data\PageInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Cms\Model\ResourceModel\Page;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PART_WOMEN_CMS_PAGES = 'extendmenu/women/women_cms_pages';
    const XML_PART_WOMEN_ROOT_CATEGORY = 'extendmenu/women/women_root_category';
    const XML_PART_WOMEN_DESIGNER_CATEGORY = 'extendmenu/women/designer_category';
    const XML_PART_WOMEN_SUB_CATEGORY = 'extendmenu/women/sub_category';
    const XML_PART_WOMEN_SALE_CATEGORY = 'extendmenu/women/sale_category';

    const XML_PART_MEN_CMS_PAGES = 'extendmenu/men/men_cms_pages';
    const XML_PART_MEN_ROOT_CATEGORY = 'extendmenu/men/men_root_category';
    const XML_PART_MEN_DESIGNER_CATEGORY = 'extendmenu/men/designer_category';
    const XML_PART_MEN_SUB_CATEGORY = 'extendmenu/men/sub_category';
    const XML_PART_MEN_SALE_CATEGORY = 'extendmenu/men/sale_category';

    protected $_scopeConfig;

    protected $_pageFactory;

    protected $_urlInterFace;

    protected $_storeManager;

    protected $_categoryFactory;

    protected $_request;

    protected $_customerSession;

    protected $_pageResource;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        PageFactory$pageFactory,
        SessionFactory $customerSession,
        StoreManagerInterface $storeManager,
        CategoryFactory $categoryFactory,
        UrlInterface $urlInterFace,
        Http $request,
        Page $pageResource
    )
    {
        $this->_customerSession = $customerSession;
        $this->_request = $request;
        $this->_urlInterFace = $urlInterFace;
        $this->_storeManager = $storeManager;
        $this->_categoryFactory = $categoryFactory;
        $this->_pageFactory = $pageFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_pageResource = $pageResource;
    }
    //config women
    public function getCmsPageWomen(){
        return $this->_scopeConfig->getValue(self::XML_PART_WOMEN_CMS_PAGES);
    }
    public function getWomenRootCategory(){
        return $this->_scopeConfig->getValue(self::XML_PART_WOMEN_ROOT_CATEGORY);
    }
    public function getDataCmsPageWomen(){
        $identifier = $this->getCmsPageWomen();
        $storeId = $this->_storeManager->getStore()->getId();
        $page = $this->_pageFactory->create();
        $page->setStoreId($storeId);
        $this->_pageResource->load($page, $identifier, PageInterface::IDENTIFIER);
        if (!$page->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $identifier));
        }
        return $page;
    }

    public function getWomenSubCategories(){
        $categoryIds = explode(',',$this->_scopeConfig->getValue(self::XML_PART_WOMEN_SUB_CATEGORY));

        $categories = [];
        foreach ($categoryIds as $categoryId) {
            $category = $this->_categoryFactory->create()->load($categoryId);
            $categories[] = $category;
        }

        return $categories;
    }


    //config men
    public function getCmsPageMen(){
        return $this->_scopeConfig->getValue(self::XML_PART_MEN_CMS_PAGES);
    }
    public function getMenRootCategory(){
        return $this->_scopeConfig->getValue(self::XML_PART_MEN_ROOT_CATEGORY);
    }
    public function getDataCmsPageMen(){
        $identifier = $this->getCmsPageMen();
        $storeId = $this->_storeManager->getStore()->getId();
        $page = $this->_pageFactory->create();
        $page->setStoreId($storeId);
        $this->_pageResource->load($page, $identifier, PageInterface::IDENTIFIER);
        if (!$page->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $identifier));
        }

        return $page;
    }
    public function getMenDesignerCategory(){
        $categoryIds = explode(',',$this->_scopeConfig->getValue(self::XML_PART_MEN_DESIGNER_CATEGORY));

        $categories = [];
        foreach ($categoryIds as $categoryId) {
            $category = $this->_categoryFactory->create()->load($categoryId);
            $categories[] = $category;
        }

        return $categories;
    }
    public function getMenSubCategories(){
        $categoryIds = explode(',',$this->_scopeConfig->getValue(self::XML_PART_MEN_SUB_CATEGORY));

        $categories = [];
        foreach ($categoryIds as $categoryId) {
            $category = $this->_categoryFactory->create()->load($categoryId);
            $categories[] = $category;
        }

        return $categories;
    }
    public function getMenSaleCategory(){
        $categoryId = $this->_scopeConfig->getValue(self::XML_PART_MEN_SALE_CATEGORY);
        $category = $this->_categoryFactory->create()->load($categoryId);

        return $category;
    }
    public function isCategoryPage(){
        $moduleName = $this->_request->getModuleName();
        $controller = $this->_request->getControllerName();
        if($moduleName == 'catalog' && $controller == 'category'){
            return true;
        }
        return false;
    }
    public function isProductPage(){
        $moduleName = $this->_request->getModuleName();
        $controller = $this->_request->getControllerName();
        if($moduleName == 'catalog' && $controller == 'product'){
            return true;
        }
        return false;
    }
    public function getUrlCmsPageWomen(){
        return $this->_urlInterFace->getUrl($this->getCmsPageWomen());
    }
    public function getUrlCmsPageMen(){
        return $this->_urlInterFace->getUrl($this->getCmsPageMen());
    }
    public function getCategoryType(){
        return $this->_customerSession->create()->getCategoryType();
    }

}
